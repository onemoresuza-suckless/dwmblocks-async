.POSIX:

PREFIX = /usr/local
CC = gcc

SCRIPTS = dbscripts

dwmblocks: main.o
	$(CC) main.o -lX11 -Ofast -o dwmblocks
main.o: main.c config.h
	$(CC) -Ofast -c main.c
clean:
	rm -f *.o *.gch dwmblocks
install: dwmblocks
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f dwmblocks $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/dwmblocks
	cd $(SCRIPTS); make install
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dwmblocks
	cd $(SCRIPTS); make uninstall

.PHONY: clean install uninstall
