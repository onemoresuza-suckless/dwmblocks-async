#define CMDLENGTH 60
#define DELIMITER " | "
#define LEADING_DELIMITER
//#define CLICKABLE_BLOCKS

const Block blocks[] = {
	BLOCK("dbscript-sysinfo -i", 1, 0),
	BLOCK("dbscript-sysinfo -d /: /var: /home:", 60, 0),
	BLOCK("dbscript-sysinfo -m", 1, 0),
	BLOCK("dbscript-sysinfo -c", 1, 0),
	BLOCK("dbscript-pulse -s", 0, 10),
	BLOCK("date '+%a %d/%m/%y at %H:%M:%S'", 1, 0)
};
